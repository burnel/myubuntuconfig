save:
	rm -rf ./config
	rm -rf ./bashrc
	mkdir config
	mkdir bashrc
	cp -r ../.config/nvim/* ./config
	cp ../.bashrc ./bashrc
	rm -rf ./config/nvim/.git*

load:
	cp -r nvim ~/.config/
	cp ./bashrc/.bashrc ~
