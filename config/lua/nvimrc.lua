local opt = vim.opt
local g = vim.g

-------------------------------------- options ------------------------------------------
opt.laststatus = 3 
opt.showmode = false

opt.clipboard = "unnamedplus"
opt.cursorline = true
opt.autochdir = true

opt.swapfile = false

opt.expandtab = true
opt.shiftwidth = 4
opt.smartindent = true
opt.tabstop = 4
opt.softtabstop = 4

opt.fillchars = { eob = " " }
opt.ignorecase = true
opt.smartcase = true

opt.scrolloff = 8
opt.number = true
opt.numberwidth = 2
opt.ruler = false
opt.relativenumber = true

opt.signcolumn = "yes"
opt.splitbelow = true
opt.splitright = true
opt.termguicolors = true
opt.timeoutlen = 400
opt.undofile = true

opt.updatetime = 250
opt.whichwrap:append "<>[]hl"

g.mapleader = " "
opt.pumheight = 8
