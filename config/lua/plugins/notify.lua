require("notify").setup({
    stages = "fade",
    timeout = 2000,
    background_colour = "#000000",
    max_width = 50,
    max_height = 10,
    minimum_width = 10,
    render = "compact",
    on_open = nil,
    on_close = nil,
})

vim.notify = require("notify")
