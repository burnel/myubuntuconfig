require("nvim-tree").setup({
    sort = {
        sorter = "case_sensitive"
    },
    view = {
        width = 30
    },
    filters = {
        dotfiles = true
    }
})

local api = require "nvim-tree.api"
vim.keymap.set('n', '<C-t>', api.tree.change_root_to_parent, {})
vim.keymap.set('n', '?',     api.tree.toggle_help, {})
vim.keymap.set('n', '<leader>to', api.tree.open, {desc="Tree Open"})
vim.keymap.set('n', '<leader>tc', api.tree.close, {desc="Tree Close"})
vim.keymap.set('n', '<leader>tt', api.tree.toggle, {desc="Tree Toggle"})
