local lsp_zero = require('lsp-zero')
lsp_zero.extend_lspconfig()

lsp_zero.on_attach(function(client, bufnr)
    lsp_zero.default_keymaps({buffer = bufnr})
end)

local lspconfig = require("lspconfig")
local capabilities = require('cmp_nvim_lsp').default_capabilities()

local language_servers = {
    lua_ls = lspconfig.lua_ls,
    clangd = lspconfig.clangd,
    jdtls = lspconfig.jdtls,
    omnisharp = lspconfig.omnisharp,
}

for server_name, server in pairs(language_servers) do
    if server_name == 'omnisharp' then
        server.setup({
            cmd = { "mono", "/opt/Omnisharp/omnisharp/OmniSharp.exe" },
            on_attach = on_attach,
            capabilities = capabilities,
            root_dir = function(fname)
                return lspconfig.util.root_pattern("Library", "Assets")(fname)
            end,
        })
    else
        server.setup({
            on_attach = on_attach,
            capabilities = capabilities,
        })
    end
end

local function show_line_diagnostics()
    local opts = {
        focusable = false,
        close_events = {"BufLeave", "CursorMoved", "InsertEnter", "FocusLost"},
        border = 'rounded',
        source = 'always',
        prefix = ' ',
    }
    vim.diagnostic.open_float(nil, opts)
end

vim.keymap.set("n", "gd", vim.lsp.buf.definition, { noremap = true, silent = true, desc = "[G]o to [D]efinition" })
vim.keymap.set("n", "gr", vim.lsp.buf.references, { noremap = true, silent = true, desc = "[G]o to [R]eferences" })
vim.keymap.set("n", "K", vim.lsp.buf.hover, { noremap = true, silent = true, desc = "Hover" })
vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, { noremap = true, silent = true, desc = "[R]e[n]ame" })
vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, { noremap = true, silent = true, desc = "[C]ode [A]ction" })
vim.keymap.set("n", "<leader>ce", show_line_diagnostics, { noremap = true, silent = true, desc = "[C]ode [E]rror" })
vim.keymap.set("n", "<leader>cs", vim.lsp.buf.signature_help, { noremap = true, silent = true, desc = "[C]ode [S]ignature" })

local cmp = require('cmp')
local luasnip = require('luasnip')

cmp.setup({
    sources = {
        { name = 'luasnip' },
        { name = 'nvim_lsp' },
    },
    window = {
        completion = {
            border = 'rounded',  -- Add rounded borders to the completion window
            winhighlight = 'Normal:FloatBorder,FloatBorder:NvimInternalBorder',  -- Highlight the border of the completion window
            pumheight = 5
        },
        documentation = {
            border = 'rounded',  -- Add rounded borders to the documentation window
            winhighlight = 'Normal:FloatBorder,FloatBorder:NvimInternalBorder',  -- Highlight the border of the documentation window
        },
    },
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    mapping = cmp.mapping.preset.insert({
        ['<Tab>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
        ['<S-Tab>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
        ['<escape>'] = cmp.mapping.abort(),
        ['<C-space>'] = cmp.mapping.complete(),
    }),
    completion = {
        completeopt = 'menu,menuone,noinsert',
        max_items = 5,
    },
})

