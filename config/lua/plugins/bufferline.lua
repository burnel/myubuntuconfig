require("bufferline").setup({
    options = {
        close_command = "bdelete! %d",
        right_mouse_command = "bdelete! %d",
        left_mouse_command = "buffer %d",
        middle_mouse_command = nil,
        indicator = {
            style = 'icon',
        },
        buffer_close_icon = '',
        modified_icon = '●',
        close_icon = '',
        left_trunc_marker = '',
        right_trunc_marker = '',
        max_name_length = 18,
        max_prefix_length = 15,
        tab_size = 24,
        diagnostics = "nvim_lsp",
        diagnostics_update_in_insert = true,
        offsets = {
            {
                filetype = "NvimTree",
                text = "File Explorer",
                text_align = "center",
                separator = true
            }
        },
        show_buffer_icons = false,
        show_buffer_close_icons = false,
        show_close_icon = false,
        show_tab_indicators = true,
        persist_buffer_sort = true,
        separator_style = "thin",
        enforce_regular_tabs = false,
        always_show_bufferline = true,
        sort_by = 'id',
    },
})

vim.keymap.set("n", "<Tab>", ":BufferLineCycleNext<CR>", { noremap = true, silent = true, desc = "Cycle to next buffer" })
vim.keymap.set("n", "<S-Tab>", ":BufferLineCyclePrev<CR>", { noremap = true, silent = true, desc = "Cycle to previous buffer" })
vim.keymap.set("n", "<Leader>x", ":bd<CR>", { noremap = true, silent = true, desc = "Close current buffer" })

vim.keymap.set('n', '&', ':BufferLineGoToBuffer 1<CR>', { noremap = true, silent = true })
vim.keymap.set('n', 'é', ':BufferLineGoToBuffer 2<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '"', ':BufferLineGoToBuffer 3<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '\'', ':BufferLineGoToBuffer 4<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '(', ':BufferLineGoToBuffer 5<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '-', ':BufferLineGoToBuffer 6<CR>', { noremap = true, silent = true })
vim.keymap.set('n', 'è', ':BufferLineGoToBuffer 7<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '_', ':BufferLineGoToBuffer 8<CR>', { noremap = true, silent = true })
vim.keymap.set('n', 'ç', ':BufferLineGoToBuffer 9<CR>', { noremap = true, silent = true })
