require("nvim-treesitter").setup({
    ensure_installed = { "c", "lua", "vim", "vimdoc", "query" },
    auto_install = true,
    ignore_install = { "javascript" },
})

