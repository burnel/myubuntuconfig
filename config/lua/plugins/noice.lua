require("noice").setup()

vim.keymap.set("n", "<leader>h", ":NoiceDismiss<CR>", { noremap = true, silent = true, desc = "[H]ide Noice" })
