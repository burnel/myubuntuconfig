# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1='\[\033[1;33m\]\u \[\033[1;37m\]: \[\033[1;32m\]\W\[\033[0m\] \$ '
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

fzf_file_search() {
    local file
    local partial_path="${READLINE_LINE:0:READLINE_POINT}"
    local base_path=$(echo "$partial_path" | sed 's/.*\s//')
    [[ -z "$base_path" ]] && base_path="."
    local dir_part=$(dirname "$base_path")
    dir_part=$(realpath "$dir_part" 2>/dev/null || echo ".")
    file=$(find "$dir_part" -type f -o -type d 2>/dev/null | fzf --height 40% --preview 'bat --style=numbers --color=always {}' --preview-window=right:60% --query="$(basename "$base_path")") || return
    READLINE_LINE="${READLINE_LINE:0:READLINE_POINT - ${#base_path}}${file}"
    READLINE_POINT=${#READLINE_LINE}
}
bind -x '"\C- ": fzf_file_search' # ctrl + space

fzf_command_history() {
    local selected_command
    selected_command=$(history | awk '{$1=""; print substr($0,2)}' | sort -u | fzf --height 100%) || return
    READLINE_LINE="${READLINE_LINE}${selected_command}"
    READLINE_POINT=${#READLINE_LINE}
}
bind -x '"\e[1;5B": fzf_command_history' # ctrl + down arrow

zq_interactive() {
    local path
    path=$(zoxide query --interactive)
    if [[ -n "$path" ]]; then
        READLINE_LINE="${READLINE_LINE:0:READLINE_POINT}$path${READLINE_LINE:READLINE_POINT}"
        READLINE_POINT=$((READLINE_POINT + ${#path}))
    fi
}
bind -x '"\e[1;5A": zq_interactive' # ctrl + up arrow


enable_conda() {
    __conda_setup="$('/home/antoine/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/home/antoine/miniconda3/etc/profile.d/conda.sh" ]; then
            . "/home/antoine/miniconda3/etc/profile.d/conda.sh"
        else
            export PATH="/home/antoine/miniconda3/bin:$PATH"
        fi
    fi
    unset __conda_setup
}
alias ccc="enable_conda"

eval $(thefuck --alias fu)
eval "$(zoxide init bash)"

export PATH="~/Downloads/apitrace/build:$PATH"
export PATH="~/Downloads/jdt-language-server/bin:$PATH"
export PATH="~/snap/dotnet-sdk/current/sdk:$PATH"
export PATH="~/snap/dotnet-sdk/current:$PATH"
export PATH="~/msbuild/artifacts/bin/MSBuild/Debug/net8.0/:$PATH"
export CXX="/usr/bin/g++"
export PATH=/usr/local/cuda-11.8/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-11.8/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

alias a="ccc && conda activate a"
alias b="c && bash"
alias blue="echo STARTING && sudo systemctl restart bluetooth && systemctl --user restart pulseaudio && echo DONE"
alias c="clear"
alias cd="z"
alias chess="firefox https://www.chess.com/home"
alias d="disown && e"
alias e="exit"
alias f="firefox"
alias fd="firefox-dev"
alias fdd="firefox-dev & d"
alias g="git"
alias gitconf="nvim ~/.gitconfig"
alias jn="jupyter notebook"
alias l="ls"
alias la="ls -a"
alias ll="ls -l"
alias lla="ls -la"
alias lsd="ls -d */"
alias lsdd="ls -d */*/"
alias m="make"
alias mc="make clean"
alias md="typora"
alias mr="make run"
alias mf="c && mc && m && mr"
alias mb="c && mc && bear -- make"
alias n="nvim"
alias naut="nautilus"
alias netflix="firefox https://www.netflix.com"
alias o="obsidian"
alias qm="qmake && m"
alias qcm="qmake && mc && m"
alias rc="nvim ~/.bashrc"
alias src="source ~/.bashrc"
alias t="tree"
alias x="xdg-open"
alias xd="xdg-open & d"

alias dre="dotnet recover"
alias db="dotnet build"
alias dr="dotnet run"
alias curl="curl -o /dev/null -s -w \"\n >> HTTP CODE : |< %{http_code} >|\n\n\""
alias ddbr="rm -f localdb.db && dotnet ef migrations remove && dotnet ef migrations add Update && dotnet ef database Update"

export PATH="$PATH:/home/antoine/.local/bin"
eval "$(register-python-argcomplete pipx)"
